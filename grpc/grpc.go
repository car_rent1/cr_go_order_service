package grpc

import (
	"gitlab.com/car_rent1/cr_go_order_service/config"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_order_service/grpc/service"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()
	order_service.RegisterOrderServiceServer(grpcServer, service.NewOrderService(cfg, log, strg, srvc))

	order_service.RegisterOrderPaymentServiceServer(grpcServer, service.NewOrderPaymentService(cfg, log, strg, srvc))
	order_service.RegisterTarifServiceServer(grpcServer, service.NewTarifService(cfg, log, strg, srvc))
	order_service.RegisterOrderPaymentServiceServer(grpcServer, service.NewOrderPaymentService(cfg, log, strg, srvc))
	order_service.RegisterGiveCarServiceServer(grpcServer, service.NewGiveCarService(cfg, log, strg, srvc))
	order_service.RegisterReceiveCarServiceServer(grpcServer, service.NewReceiveCarService(cfg, log, strg, srvc))
	
	reflection.Register(grpcServer)
	return
}

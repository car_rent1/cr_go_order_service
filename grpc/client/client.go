package client

import (
	"gitlab.com/car_rent1/cr_go_order_service/config"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	CarService() car_service.CarServiceClient
	UserService() user_service.InvestorServiceClient
	BranchService() user_service.BranchServiceClient
	CarActivityService() car_service.CarActivityServiceClient
}

type grpcClients struct {
	carService         car_service.CarServiceClient
	userService        user_service.InvestorServiceClient
	branchService      user_service.BranchServiceClient
	carActivityService car_service.CarActivityServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	connCarService, err := grpc.Dial(
		cfg.CARServiceHost+cfg.CARGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	connUserService, err := grpc.Dial(
		cfg.USERServiceHost+cfg.USERGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	connBranchService, err := grpc.Dial(
		cfg.USERServiceHost+cfg.USERGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	connCarActivityService, err := grpc.Dial(
		cfg.CARServiceHost+cfg.CARGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	return &grpcClients{
		carService:         car_service.NewCarServiceClient(connCarService),
		userService:        user_service.NewInvestorServiceClient(connUserService),
		branchService:      user_service.NewBranchServiceClient(connBranchService),
		carActivityService: car_service.NewCarActivityServiceClient(connCarActivityService),
	}, nil
}

func (g *grpcClients) CarService() car_service.CarServiceClient {
	return g.carService
}
func (g *grpcClients) UserService() user_service.InvestorServiceClient {
	return g.userService
}
func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.branchService
}
func (g *grpcClients) CarActivityService() car_service.CarActivityServiceClient {
	return g.carActivityService
}

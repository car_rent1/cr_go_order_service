package service

//drbqsfktvgggzulr
import (
	"context"
	"log"
	"net/smtp"

	"gitlab.com/car_rent1/cr_go_order_service/config"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/user_service"
	"gitlab.com/car_rent1/cr_go_order_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type OrderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedOrderServiceServer
	*car_service.UnimplementedCarServiceServer
}

func NewOrderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *OrderService {
	return &OrderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *OrderService) Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.Order, err error) {

	i.log.Info("---CreateOrder------>", logger.Any("req", req))

	pKey, err := i.strg.Order().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateOrder->Order->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Order().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	car, err := i.services.CarService().GetByID(ctx, &car_service.CarPrimaryKey{Id: resp.CarId})
	if err != nil {
		i.log.Error("!!!CreateOrder->Car->GetById--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	cars, err := i.services.CarService().Update(ctx, &car_service.UpdateCar{
		Id:          car.Id,
		BrandId:     car.BrandId,
		ModelId:     car.ModelId,
		StateNumber: car.StateNumber,
		Milleage:    car.Milleage,
		InvestorId:  car.InvestorId,
		Status:      "BOOKED",
	})

	if err != nil {
		i.log.Error("!!!CreateOrder->Car->UpdateCar--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	_, err = i.services.CarActivityService().Create(ctx, &car_service.CarActivityCreate{
		StateNumber: cars.StateNumber,
		CarId:       cars.Id,
		Status:      cars.Status,
		Date:        cars.UpdatedAt,
	})
	if err != nil {
		i.log.Error("!!!CreateOrder->CarActivity->CreateCarActivity--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	_, err = i.services.CarActivityService().Create(ctx, &car_service.CarActivityCreate{
		StateNumber: cars.StateNumber,
		CarId:       cars.Id,
		Status:      cars.Status,
		Date:        cars.UpdatedAt,
	})
	if err != nil {
		i.log.Error("!!!CreateOrder->CarActivity->CreateCarActivity--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	msg := "your order has been created!!!"
	i.sendMailForCreate(msg)
	return
}
func (i *OrderService) GetByID(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error) {

	i.log.Info("---GetOrderByID------>", logger.Any("req", req))

	resp, err = i.strg.Order().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOrderByID->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderService) GetList(ctx context.Context, req *order_service.GetListOrderRequest) (*order_service.GetListOrderResponse, error) {
	i.log.Info("-------GetListOrder-------", logger.Any("req", req))

	resp, err := i.strg.Order().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetList->Order->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *OrderService) Update(ctx context.Context, req *order_service.UpdateOrder) (resp *order_service.Order, err error) {
	i.log.Info("---UpdateInvestor------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Order().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateInvestor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Order().GetByPKey(ctx, &order_service.OrderPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!UpdateOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}
	car, err := i.services.CarService().GetByID(ctx, &car_service.CarPrimaryKey{Id: resp.CarId})
	if err != nil {
		i.log.Error("!!!CreateOrder->Car->GetById--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	if resp.Status == "CANCEL" {
		cars, err := i.services.CarService().Update(ctx, &car_service.UpdateCar{
			Id:          car.Id,
			BrandId:     car.BrandId,
			ModelId:     car.ModelId,
			StateNumber: car.StateNumber,
			Milleage:    car.Milleage,
			InvestorId:  car.InvestorId,
			Status:      "IN_STOCK",
		})
		if err != nil {
			i.log.Error("!!!UpdateOrder->CarService->UpdateCarService--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		_, err = i.services.CarActivityService().Create(ctx, &car_service.CarActivityCreate{
			StateNumber: cars.StateNumber,
			CarId:       cars.Id,
			Status:      cars.Status,
			Date:        cars.UpdatedAt,
		})
		if err != nil {
			i.log.Error("!!!CreateOrder->CarActivity->CreateCarActivity--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		investor, err := i.services.UserService().GetByID(ctx, &user_service.InvestorPrimaryKey{Id: car.InvestorId})
		if err != nil {
			i.log.Error("!!!UpdateOrder->Investor->getById--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		order_tarif, err := i.strg.OrderTarif().GetByPKey(ctx, &order_service.OrderTarifPrimaryKey{Id: resp.Id})
		if err != nil {
			i.log.Error("!!!UpdateOrder->OrderTarif->getById--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		iBalance := investor.Balance - float32(order_tarif.TarifPayment)*70/100
		_, err = i.services.UserService().Update(ctx, &user_service.UpdateInvestor{
			Id:          investor.Id,
			FirstName:   investor.FirstName,
			LastName:    investor.LastName,
			DateOfBirth: investor.DateOfBirth,
			PhoneNumber: investor.PhoneNumber,
			Balance:     iBalance,
		})
		if err != nil {
			i.log.Error("!!!UpdateOrder->OrderTarif->Update--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		branch, err := i.services.BranchService().GetByID(ctx, &user_service.BranchPrimaryKey{Id: resp.BranchId})
		if err != nil {
			i.log.Error("!!!UpdateOrder->Branch->GetById--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		bBalance := branch.Balance - float64(order_tarif.TarifPayment)*30/100
		_, err = i.services.BranchService().Update(ctx, &user_service.UpdateBranch{

			Id:          branch.Id,
			Name:        branch.Name,
			Address:     branch.Address,
			PhoneNumber: branch.PhoneNumber,
			Balance:     bBalance,
		})
		if err != nil {
			i.log.Error("!!!UpdateOrder->Branch->Update--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		msg := "your order has been canceled !!!"
		i.sendMailForCreate(msg)
	} else if resp.Status == "CLIENT_TOOK" {
		cars, err := i.services.CarService().Update(ctx, &car_service.UpdateCar{
			Id:          car.Id,
			BrandId:     car.BrandId,
			ModelId:     car.ModelId,
			StateNumber: car.StateNumber,
			Milleage:    car.Milleage,
			InvestorId:  car.InvestorId,
			Status:      "IN_USE",
		})
		if err != nil {
			i.log.Error("!!!UpdateOrder->CarService->UpdateCarService--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		_, err = i.services.CarActivityService().Create(ctx, &car_service.CarActivityCreate{
			StateNumber: cars.StateNumber,
			CarId:       cars.Id,
			Status:      cars.Status,
			Date:        cars.UpdatedAt,
		})
		if err != nil {
			i.log.Error("!!!CreateOrder->CarActivity->CreateCarActivity--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		msg := "You Successfully took car !!!"
		i.sendMailForCreate(msg)
	} else if resp.Status == "CLIENT_RETURNED" {
		cars, err := i.services.CarService().Update(ctx, &car_service.UpdateCar{
			Id:          car.Id,
			BrandId:     car.BrandId,
			ModelId:     car.ModelId,
			StateNumber: car.StateNumber,
			Milleage:    car.Milleage,
			InvestorId:  car.InvestorId,
			Status:      "IN_STOCK",
		})
		if err != nil {
			i.log.Error("!!!UpdateOrder->CarService->UpdateCarService--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		_, err = i.services.CarActivityService().Create(ctx, &car_service.CarActivityCreate{
			StateNumber: cars.StateNumber,
			CarId:       cars.Id,
			Status:      cars.Status,
			Date:        cars.UpdatedAt,
		})
		if err != nil {
			i.log.Error("!!!CreateOrder->CarActivity->CreateCarActivity--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		msg := "You Successfully Received car !!!"
		i.sendMailForCreate(msg)
	} else if resp.Status == "FINISHED" {
		cars, err := i.services.CarService().Update(ctx, &car_service.UpdateCar{
			Id:          car.Id,
			BrandId:     car.BrandId,
			ModelId:     car.ModelId,
			StateNumber: car.StateNumber,
			Milleage:    car.Milleage,
			InvestorId:  car.InvestorId,
			Status:      "IN_STOCK",
		})
		if err != nil {
			i.log.Error("!!!UpdateOrder->CarService->UpdateCarService--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		_, err = i.services.CarActivityService().Create(ctx, &car_service.CarActivityCreate{
			StateNumber: cars.StateNumber,
			CarId:       cars.Id,
			Status:      cars.Status,
			Date:        cars.UpdatedAt,
		})
		if err != nil {
			i.log.Error("!!!CreateOrder->CarActivity->CreateCarActivity--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		msg := "Your Order Has Been Finished !!!"
		i.sendMailForCreate(msg)
	}

	return resp, err
}

func (i *OrderService) Delete(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.OrderEmpty, err error) {

	i.log.Info("---DeleteOrder------>", logger.Any("req", req))

	resp, err = i.strg.Order().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteOrder->Order->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *OrderService) sendMailForCreate(msg string) {
	auth := smtp.PlainAuth(
		"",
		"shohruxramazonov564@gmail.com",
		"drbqsfktvgggzulr",
		"smtp.gmail.com",
	)
	err := smtp.SendMail(
		"smtp.gmail.com:587",
		auth,
		"shohruxramazonov564@gmail.com",
		[]string{"shohruxramazonov564@gmail.com"},
		[]byte(msg),
	)
	if err != nil {
		log.Println(err)
	}
}

package service

import (
	"context"
	"log"
	"net/smtp"

	"gitlab.com/car_rent1/cr_go_order_service/config"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GiveCarService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedGiveCarServiceServer
	*car_service.UnimplementedCarServiceServer
}

func NewGiveCarService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *GiveCarService {
	return &GiveCarService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *GiveCarService) Create(ctx context.Context, req *order_service.CreateGiveCar) (resp *order_service.GiveCar, err error) {
	i.log.Info("---CreateGiveCar------>", logger.Any("req", req))

	pKey, err := i.strg.GiveCar().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateGiveCar->GiveCar->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	order, err := i.strg.Order().GetByPKey(ctx, &order_service.OrderPrimaryKey{
		Id: req.OrderId,
	})

	if err != nil {
		i.log.Error("!!!GetByPKeyOrder->Order Data->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	_, err = i.strg.Order().Update(ctx, &order_service.UpdateOrder{
		Id:          order.Id,
		TarifId:     order.TarifId,
		ClientId:    order.ClientId,
		Date:        order.Date,
		Duration:    order.Duration,
		Extend:      order.Extend,
		Discount:    order.Discount,
		Insurance:   order.Insurance,
		BranchId:    order.BranchId,
		Status:      "client_took",
		OrderNumber: order.OrderNumber,
		Payd:        !order.Payd,
		CarId:       order.CarId,
	})
	if err != nil {
		i.log.Error("!!!UpdateOrder-> Give Car ->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	carData, err := i.services.CarService().GetByID(ctx, &car_service.CarPrimaryKey{
		Id: order.CarId,
	})
	if err != nil {
		i.log.Error("!!!GetByPKeyCar->GiveCar->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	_, err = i.services.CarService().Update(ctx, &car_service.UpdateCar{
		Id:          carData.Id,
		BrandId:     carData.BrandId,
		ModelId:     carData.ModelId,
		StateNumber: carData.StateNumber,
		Milleage:    req.Mileage,
		InvestorId:  carData.InvestorId,
	})

	if err != nil {
		i.log.Error("!!!UpdateCar-> Give Car ->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.GiveCar().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyGiveCar->GiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	i.sendMailForCreate("Car successfully took")
	return
}
func (i *GiveCarService) GetByID(ctx context.Context, req *order_service.GiveCarPrimaryKey) (resp *order_service.GiveCar, err error) {

	i.log.Info("---GetGiveCarByID------>", logger.Any("req", req))

	resp, err = i.strg.GiveCar().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetGiveCarByID->GiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *GiveCarService) GetList(ctx context.Context, req *order_service.GetListGiveCarRequest) (*order_service.GetListGiveCarResponse, error) {
	i.log.Info("-------GetListGiveCar-------", logger.Any("req", req))

	resp, err := i.strg.GiveCar().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetList->GiveCar->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *GiveCarService) Update(ctx context.Context, req *order_service.UpdateGiveCar) (resp *order_service.GiveCar, err error) {
	i.log.Info("---UpdateInvestor------>", logger.Any("req", req))

	rowsAffected, err := i.strg.GiveCar().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateInvestor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.GiveCar().GetByPKey(ctx, &order_service.GiveCarPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!UpdateGiveCar->GiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *GiveCarService) Delete(ctx context.Context, req *order_service.GiveCarPrimaryKey) (resp *order_service.GiveCarEmpty, err error) {

	i.log.Info("---DeleteGiveCar------>", logger.Any("req", req))

	resp, err = i.strg.GiveCar().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteGiveCar->GiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *GiveCarService) sendMailForCreate(msg string) {
	auth := smtp.PlainAuth(
		"",
		"shohruxramazonov564@gmail.com",
		"CAR24",
		"smtp.gmail.com",
	)
	err := smtp.SendMail(
		"smtp.gmail.com:587",
		auth,
		"shohruxramazonov564@gmail.com",
		[]string{"shohruxramazonov564@gmail.com"},
		[]byte(msg),
	)
	if err != nil {
		log.Println(err)
	}
}

package service

import (
	"context"
	"log"
	"net/smtp"

	"gitlab.com/car_rent1/cr_go_order_service/config"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ReceiveCarService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*car_service.UnimplementedCarServiceServer
	*order_service.UnimplementedReceiveCarServiceServer
}

func NewReceiveCarService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ReceiveCarService {
	return &ReceiveCarService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ReceiveCarService) Create(ctx context.Context, req *order_service.CreateReceiveCar) (resp *order_service.ReceiveCar, err error) {
	i.log.Info("---CreateReceiveCar------>", logger.Any("req", req))

	pKey, err := i.strg.ReceiveCar().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateReceiveCar->ReceiveCar->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	order, err := i.strg.Order().GetByPKey(ctx, &order_service.OrderPrimaryKey{
		Id: req.OrderId,
	})

	if err != nil {
		i.log.Error("!!!GetByPKeyOrder->Order Data->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	_, err = i.strg.Order().Update(ctx, &order_service.UpdateOrder{
		Id:          order.Id,
		TarifId:     order.TarifId,
		ClientId:    order.ClientId,
		Date:        order.Date,
		Duration:    order.Duration,
		Extend:      order.Extend,
		Discount:    order.Discount,
		Insurance:   order.Insurance,
		BranchId:    order.BranchId,
		Status:      "CLIENT_RETURNED",
		OrderNumber: order.OrderNumber,
		Payd:        !order.Payd,
		CarId:       order.CarId,
	})
	if err != nil {
		i.log.Error("!!!UpdateOrder-> Receive Car ->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	carData, err := i.services.CarService().GetByID(ctx, &car_service.CarPrimaryKey{
		Id: order.CarId,
	})
	if err != nil {
		i.log.Error("!!!GetByPKeyCar->Receive Car->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	_, err = i.services.CarService().Update(ctx, &car_service.UpdateCar{
		Id:          carData.Id,
		BrandId:     carData.BrandId,
		ModelId:     carData.ModelId,
		StateNumber: carData.StateNumber,
		Milleage:    req.Mileage,
		InvestorId:  carData.InvestorId,
	})

	if err != nil {
		i.log.Error("!!!UpdateCar-> Receive Car ->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	tarifData, err := i.strg.Tarif().GetByPKey(ctx, &order_service.TarifPrimaryKey{
		Id: order.TarifId,
	})
	if err != nil {
		i.log.Error("!!!Get Tarif Data-> Receive Car ->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if req.Mileage-carData.Milleage > tarifData.DayLimit {
		_, err = i.strg.Deptor().Create(ctx, &order_service.CreateDeptor{
			OrderId: order.Id,
			DeptSum: (req.Mileage - carData.Milleage) * tarifData.OverLimit,
			Status: "true",
		})
	}else{
		_, err = i.strg.Order().Update(ctx, &order_service.UpdateOrder{
			Id:          order.Id,
			TarifId:     order.TarifId,
			ClientId:    order.ClientId,
			Date:        order.Date,
			Duration:    order.Duration,
			Extend:      order.Extend,
			Discount:    order.Discount,
			Insurance:   order.Insurance,
			BranchId:    order.BranchId,
			Status:      "Finished",
			OrderNumber: order.OrderNumber,
			Payd:        !order.Payd,
			CarId:       order.CarId,
		})
		if err != nil {
			i.log.Error("!!!UpdateOrder-> Receive Car ->Create--->", logger.Error(err))
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		i.sendMailForCreate("You must extra pay")
	}

	resp, err = i.strg.ReceiveCar().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyReceiveCar->ReceiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	i.sendMailForCreate("Car successfully took")

	return
}
func (i *ReceiveCarService) GetByID(ctx context.Context, req *order_service.ReceiveCarPrimaryKey) (resp *order_service.ReceiveCar, err error) {

	i.log.Info("---GetReceiveCarByID------>", logger.Any("req", req))

	resp, err = i.strg.ReceiveCar().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetReceiveCarByID->ReceiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ReceiveCarService) GetList(ctx context.Context, req *order_service.GetListReceiveCarRequest) (*order_service.GetListReceiveCarResponse, error) {
	i.log.Info("-------GetListReceiveCar-------", logger.Any("req", req))

	resp, err := i.strg.ReceiveCar().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetList->ReceiveCar->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *ReceiveCarService) Update(ctx context.Context, req *order_service.UpdateReceiveCar) (resp *order_service.ReceiveCar, err error) {
	i.log.Info("---UpdateInvestor------>", logger.Any("req", req))

	rowsAffected, err := i.strg.ReceiveCar().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateInvestor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ReceiveCar().GetByPKey(ctx, &order_service.ReceiveCarPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!UpdateReceiveCar->ReceiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ReceiveCarService) Delete(ctx context.Context, req *order_service.ReceiveCarPrimaryKey) (resp *order_service.ReceiveCarEmpty, err error) {

	i.log.Info("---DeleteReceiveCar------>", logger.Any("req", req))

	resp, err = i.strg.ReceiveCar().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteReceiveCar->ReceiveCar->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *ReceiveCarService) sendMailForCreate(msg string) {
	auth := smtp.PlainAuth(
		"",
		"shohruxramazonov564@gmail.com",
		"CAR24",
		"smtp.gmail.com",
	)
	err := smtp.SendMail(
		"smtp.gmail.com:587",
		auth,
		"shohruxramazonov564@gmail.com",
		[]string{"shohruxramazonov564@gmail.com"},
		[]byte(msg),
	)
	if err != nil {
		log.Println(err)
	}
}

package service

//drbqsfktvgggzulr
import (
	"context"

	"gitlab.com/car_rent1/cr_go_order_service/config"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeptorService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedDeptorServiceServer
	*car_service.UnimplementedCarServiceServer
}

func NewDeptorService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *DeptorService {
	return &DeptorService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *DeptorService) Create(ctx context.Context, req *order_service.CreateDeptor) (resp *order_service.Deptor, err error) {

	i.log.Info("---CreateDeptor------>", logger.Any("req", req))

	pKey, err := i.strg.Deptor().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateDeptor->Deptor->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Deptor().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyDeptor->Deptor->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}
func (i *DeptorService) GetByID(ctx context.Context, req *order_service.DeptorPrimaryKey) (resp *order_service.Deptor, err error) {

	i.log.Info("---GetDeptorByID------>", logger.Any("req", req))

	resp, err = i.strg.Deptor().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetDeptorByID->Deptor->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *DeptorService) GetList(ctx context.Context, req *order_service.GetListDeptorRequest) (*order_service.GetListDeptorResponse, error) {
	i.log.Info("-------GetListDeptor-------", logger.Any("req", req))

	resp, err := i.strg.Deptor().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetList->Deptor->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *DeptorService) Update(ctx context.Context, req *order_service.UpdateDeptor) (resp *order_service.Deptor, err error) {
	i.log.Info("---UpdateInvestor------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Deptor().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateInvestor--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Deptor().GetByPKey(ctx, &order_service.DeptorPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!UpdateDeptor->Deptor->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *DeptorService) Delete(ctx context.Context, req *order_service.DeptorPrimaryKey) (resp *order_service.DeptorEmpty, err error) {

	i.log.Info("---DeleteDeptor------>", logger.Any("req", req))

	resp, err = i.strg.Deptor().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteDeptor->Deptor->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

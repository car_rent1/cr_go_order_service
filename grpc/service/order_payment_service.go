package service

//drbqsfktvgggzulr
import (
	"context"
	"log"
	"net/smtp"

	"gitlab.com/car_rent1/cr_go_order_service/config"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/car_service"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/user_service"
	"gitlab.com/car_rent1/cr_go_order_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type OrderPaymentService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedOrderPaymentServiceServer
	*car_service.UnimplementedCarServiceServer
	*user_service.UnimplementedInvestorServiceServer
}

func NewOrderPaymentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *OrderPaymentService {
	return &OrderPaymentService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *OrderPaymentService) Create(ctx context.Context, req *order_service.CreateOrderPayment) (resp *order_service.OrderPayment, err error) {

	i.log.Info("---CreateOrderPayment------>", logger.Any("req", req))

	var (
		insurance     int
		investorMoney int
		allMoney      int
		companyMoney  int
		msg           = "Order payment succesfully.\n Thank you for your Order"
	)

	pKey, err := i.strg.OrderPayment().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateOrderPayment->OrderPayment->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.OrderPayment().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!CreateOrderPayment->OrderPaymentGetBYPkey->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	allMoney = int(resp.Cash + resp.Click + resp.Humo + resp.Payme + resp.Uzcard)

	order, err := i.strg.Order().GetByPKey(ctx, &order_service.OrderPrimaryKey{Id: resp.OrderId})
	if err != nil {
		i.log.Error("!!!CreateOrderPayment->Order->GetByPkey--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	tarif, err := i.strg.Tarif().GetByPKey(ctx, &order_service.TarifPrimaryKey{Id: order.TarifId})
	if err != nil {
		i.log.Error("!!!CreateOrderPayment->Tarif->GetByPkey--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	car, err := i.services.CarService().GetByID(ctx, &car_service.CarPrimaryKey{Id: order.CarId})
	if err != nil {
		i.log.Error("!!!CreateOrderPayment-> Carservice ->GetByPkey--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	investor, err := i.services.UserService().GetByID(ctx, &user_service.InvestorPrimaryKey{Id: car.InvestorId})
	if err != nil {
		i.log.Error("!!!CreateOrderPayment-> UserService ->GetByPkey--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	branch, err := i.services.BranchService().GetByID(ctx, &user_service.BranchPrimaryKey{Id: order.BranchId})
	if err != nil {
		i.log.Error("!!!CreateOrderPayment-> BranchService ->GetByPkey--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if order.Insurance {
		insurance = int(tarif.InsurancePrice) * int(order.Duration)
	}

	investor.Balance += float32(allMoney) * 70 / 100
	investorMoney = int(investor.Balance)
	_, err = i.services.UserService().Update(ctx, &user_service.UpdateInvestor{
		Id:          investor.Id,
		FirstName:   investor.FirstName,
		LastName:    investor.LastName,
		DateOfBirth: investor.DateOfBirth,
		Balance:     investor.Balance,
	})
	if err != nil {
		i.log.Error("!!!CreateOrderPayment-> UserService ->Update--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	companyMoney = allMoney - investorMoney - insurance
	branch.Balance = branch.Balance + float64(allMoney-investorMoney) - float64(insurance)

	_, err = i.services.BranchService().Update(ctx, &user_service.UpdateBranch{
		Id:          branch.Id,
		Name:        branch.Name,
		Address:     branch.Address,
		PhoneNumber: branch.PhoneNumber,
		Balance:     branch.Balance,
	})
	if err != nil {
		i.log.Error("!!!CreateOrderPayment-> BranchService ->Update--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	_, err = i.strg.OrderTarif().Create(ctx, &order_service.CreateOrderTarif{
		TarifPayment: int64(allMoney),
		Investor:     int64(investorMoney),
		Insurance:    int64(insurance),
		Company:      int64(companyMoney),
		Day:          order.Duration,
		OrderId:      order.Id,
	})
	if err != nil {
		i.log.Error("!!!CreateOrderPayment-> OrderTarif ->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	_, err = i.strg.Order().Update(ctx, &order_service.UpdateOrder{
		Id:          order.Id,
		TarifId:     order.TarifId,
		ClientId:    order.ClientId,
		Date:        order.Date,
		Duration:    order.Duration,
		Extend:      order.Extend,
		Discount:    order.Discount,
		Insurance:   order.Insurance,
		BranchId:    order.BranchId,
		Status:      order.Status,
		OrderNumber: order.OrderNumber,
		Payd:        !order.Payd,
		CarId:       order.CarId,
	})
	if err != nil {
		i.log.Error("!!!CreateOrderPayment-> Order ->Update--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	i.sendMailForPayment(msg)

	return
}
func (i *OrderPaymentService) GetByID(ctx context.Context, req *order_service.OrderPaymentPrimaryKey) (resp *order_service.OrderPayment, err error) {

	i.log.Info("---GetOrderPaymentByID------>", logger.Any("req", req))

	resp, err = i.strg.OrderPayment().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOrderByID->OrderPayment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *OrderPaymentService) GetList(ctx context.Context, req *order_service.GetListOrderPaymentRequest) (*order_service.GetListOrderPaymentResponse, error) {
	i.log.Info("-------GetListOrderPayment-------", logger.Any("req", req))

	resp, err := i.strg.OrderPayment().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListOrderPayment->OrderPayment->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *OrderPaymentService) Update(ctx context.Context, req *order_service.UpdateOrderPayment) (resp *order_service.OrderPayment, err error) {
	i.log.Info("---UpdateOrderPayment------>", logger.Any("req", req))

	rowsAffected, err := i.strg.OrderPayment().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateOrderPayment--->Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}
	return
}

func (i *OrderPaymentService) Delete(ctx context.Context, req *order_service.OrderPaymentPrimaryKey) (resp *order_service.OrderPaymentEmpty, err error) {

	i.log.Info("---DeleteOrderPayment------>", logger.Any("req", req))

	resp, err = i.strg.OrderPayment().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteOrderPayment->OrderPayment->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (i *OrderPaymentService) sendMailForPayment(msg string) {
	auth := smtp.PlainAuth(
		"",
		"arzubaev04@gmail.com",
		"drbqsfktvgggzulr",
		"smtp.gmail.com",
	)
	err := smtp.SendMail(
		"smtp.gmail.com:587",
		auth,
		"arzubaev04@gmail.com",
		[]string{"arzubaev04@gmail.com"},
		[]byte(msg),
	)
	if err != nil {
		log.Println(err)
	}
}

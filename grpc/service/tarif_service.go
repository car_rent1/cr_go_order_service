package service

//drbqsfktvgggzulr
import (
	"context"

	"gitlab.com/car_rent1/cr_go_order_service/config"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/grpc/client"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/logger"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type TarifService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*order_service.UnimplementedTarifServiceServer
}

func NewTarifService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *TarifService {
	return &TarifService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *TarifService) Create(ctx context.Context, req *order_service.CreateTarif) (resp *order_service.Tarif, err error) {

	i.log.Info("---CreateTarif------>", logger.Any("req", req))

	pKey, err := i.strg.Tarif().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateTarif->Tarif->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Tarif().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyTarif->Tarif->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}
func (i *TarifService) GetByID(ctx context.Context, req *order_service.TarifPrimaryKey) (resp *order_service.Tarif, err error) {

	i.log.Info("---GetTarifByID------>", logger.Any("req", req))

	resp, err = i.strg.Tarif().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetOrderByID->Tarif->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *TarifService) GetList(ctx context.Context, req *order_service.GetListTarifRequest) (*order_service.GetListTarifResponse, error) {
	i.log.Info("-------GetListTarif-------", logger.Any("req", req))

	resp, err := i.strg.Tarif().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetListTarif->Tarif->GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return resp, nil
}

func (i *TarifService) Update(ctx context.Context, req *order_service.UpdateTarif) (resp *order_service.Tarif, err error) {
	i.log.Info("---UpdateTarif------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Tarif().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateTarif--->Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Tarif().GetByPKey(ctx, &order_service.TarifPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!UpdateTarif ---> GetByPkey ")
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	return
}

func (i *TarifService) Delete(ctx context.Context, req *order_service.TarifPrimaryKey) (resp *order_service.TarifEmpty, err error) {

	i.log.Info("---DeleteTarif------>", logger.Any("req", req))

	resp, err = i.strg.Tarif().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteTarif->Tarif->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.21.12
// source: give_car.proto

package order_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type GiveCar struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	OrderId    string `protobuf:"bytes,2,opt,name=order_id,json=orderId,proto3" json:"order_id,omitempty"`
	MechanicId string `protobuf:"bytes,3,opt,name=mechanic_id,json=mechanicId,proto3" json:"mechanic_id,omitempty"`
	Mileage    int64  `protobuf:"varint,4,opt,name=mileage,proto3" json:"mileage,omitempty"`
	Gas        int64  `protobuf:"varint,5,opt,name=gas,proto3" json:"gas,omitempty"`
	Photo      string `protobuf:"bytes,6,opt,name=photo,proto3" json:"photo,omitempty"`
	CreatedAt  string `protobuf:"bytes,7,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt  string `protobuf:"bytes,8,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *GiveCar) Reset() {
	*x = GiveCar{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_car_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GiveCar) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GiveCar) ProtoMessage() {}

func (x *GiveCar) ProtoReflect() protoreflect.Message {
	mi := &file_give_car_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GiveCar.ProtoReflect.Descriptor instead.
func (*GiveCar) Descriptor() ([]byte, []int) {
	return file_give_car_proto_rawDescGZIP(), []int{0}
}

func (x *GiveCar) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *GiveCar) GetOrderId() string {
	if x != nil {
		return x.OrderId
	}
	return ""
}

func (x *GiveCar) GetMechanicId() string {
	if x != nil {
		return x.MechanicId
	}
	return ""
}

func (x *GiveCar) GetMileage() int64 {
	if x != nil {
		return x.Mileage
	}
	return 0
}

func (x *GiveCar) GetGas() int64 {
	if x != nil {
		return x.Gas
	}
	return 0
}

func (x *GiveCar) GetPhoto() string {
	if x != nil {
		return x.Photo
	}
	return ""
}

func (x *GiveCar) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *GiveCar) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type CreateGiveCar struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	OrderId    string `protobuf:"bytes,1,opt,name=order_id,json=orderId,proto3" json:"order_id,omitempty"`
	MechanicId string `protobuf:"bytes,2,opt,name=mechanic_id,json=mechanicId,proto3" json:"mechanic_id,omitempty"`
	Mileage    int64  `protobuf:"varint,3,opt,name=mileage,proto3" json:"mileage,omitempty"`
	Gas        int64  `protobuf:"varint,4,opt,name=gas,proto3" json:"gas,omitempty"`
	Photo      string `protobuf:"bytes,5,opt,name=photo,proto3" json:"photo,omitempty"`
}

func (x *CreateGiveCar) Reset() {
	*x = CreateGiveCar{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_car_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateGiveCar) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateGiveCar) ProtoMessage() {}

func (x *CreateGiveCar) ProtoReflect() protoreflect.Message {
	mi := &file_give_car_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateGiveCar.ProtoReflect.Descriptor instead.
func (*CreateGiveCar) Descriptor() ([]byte, []int) {
	return file_give_car_proto_rawDescGZIP(), []int{1}
}

func (x *CreateGiveCar) GetOrderId() string {
	if x != nil {
		return x.OrderId
	}
	return ""
}

func (x *CreateGiveCar) GetMechanicId() string {
	if x != nil {
		return x.MechanicId
	}
	return ""
}

func (x *CreateGiveCar) GetMileage() int64 {
	if x != nil {
		return x.Mileage
	}
	return 0
}

func (x *CreateGiveCar) GetGas() int64 {
	if x != nil {
		return x.Gas
	}
	return 0
}

func (x *CreateGiveCar) GetPhoto() string {
	if x != nil {
		return x.Photo
	}
	return ""
}

type UpdateGiveCar struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	OrderId    string `protobuf:"bytes,2,opt,name=order_id,json=orderId,proto3" json:"order_id,omitempty"`
	MechanicId string `protobuf:"bytes,3,opt,name=mechanic_id,json=mechanicId,proto3" json:"mechanic_id,omitempty"`
	Mileage    int64  `protobuf:"varint,4,opt,name=mileage,proto3" json:"mileage,omitempty"`
	Gas        int64  `protobuf:"varint,5,opt,name=gas,proto3" json:"gas,omitempty"`
	Photo      string `protobuf:"bytes,6,opt,name=photo,proto3" json:"photo,omitempty"`
}

func (x *UpdateGiveCar) Reset() {
	*x = UpdateGiveCar{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_car_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateGiveCar) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateGiveCar) ProtoMessage() {}

func (x *UpdateGiveCar) ProtoReflect() protoreflect.Message {
	mi := &file_give_car_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateGiveCar.ProtoReflect.Descriptor instead.
func (*UpdateGiveCar) Descriptor() ([]byte, []int) {
	return file_give_car_proto_rawDescGZIP(), []int{2}
}

func (x *UpdateGiveCar) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateGiveCar) GetOrderId() string {
	if x != nil {
		return x.OrderId
	}
	return ""
}

func (x *UpdateGiveCar) GetMechanicId() string {
	if x != nil {
		return x.MechanicId
	}
	return ""
}

func (x *UpdateGiveCar) GetMileage() int64 {
	if x != nil {
		return x.Mileage
	}
	return 0
}

func (x *UpdateGiveCar) GetGas() int64 {
	if x != nil {
		return x.Gas
	}
	return 0
}

func (x *UpdateGiveCar) GetPhoto() string {
	if x != nil {
		return x.Photo
	}
	return ""
}

type GiveCarPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *GiveCarPrimaryKey) Reset() {
	*x = GiveCarPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_car_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GiveCarPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GiveCarPrimaryKey) ProtoMessage() {}

func (x *GiveCarPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_give_car_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GiveCarPrimaryKey.ProtoReflect.Descriptor instead.
func (*GiveCarPrimaryKey) Descriptor() ([]byte, []int) {
	return file_give_car_proto_rawDescGZIP(), []int{3}
}

func (x *GiveCarPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type GetListGiveCarRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *GetListGiveCarRequest) Reset() {
	*x = GetListGiveCarRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_car_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListGiveCarRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListGiveCarRequest) ProtoMessage() {}

func (x *GetListGiveCarRequest) ProtoReflect() protoreflect.Message {
	mi := &file_give_car_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListGiveCarRequest.ProtoReflect.Descriptor instead.
func (*GetListGiveCarRequest) Descriptor() ([]byte, []int) {
	return file_give_car_proto_rawDescGZIP(), []int{4}
}

func (x *GetListGiveCarRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListGiveCarRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetListGiveCarRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type GetListGiveCarResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count    int64      `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	GiveCars []*GiveCar `protobuf:"bytes,2,rep,name=GiveCars,proto3" json:"GiveCars,omitempty"`
}

func (x *GetListGiveCarResponse) Reset() {
	*x = GetListGiveCarResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_give_car_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListGiveCarResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListGiveCarResponse) ProtoMessage() {}

func (x *GetListGiveCarResponse) ProtoReflect() protoreflect.Message {
	mi := &file_give_car_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListGiveCarResponse.ProtoReflect.Descriptor instead.
func (*GetListGiveCarResponse) Descriptor() ([]byte, []int) {
	return file_give_car_proto_rawDescGZIP(), []int{5}
}

func (x *GetListGiveCarResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *GetListGiveCarResponse) GetGiveCars() []*GiveCar {
	if x != nil {
		return x.GiveCars
	}
	return nil
}

var File_give_car_proto protoreflect.FileDescriptor

var file_give_car_proto_rawDesc = []byte{
	0x0a, 0x0e, 0x67, 0x69, 0x76, 0x65, 0x5f, 0x63, 0x61, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x12, 0x0d, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x22,
	0xd5, 0x01, 0x0a, 0x07, 0x47, 0x69, 0x76, 0x65, 0x43, 0x61, 0x72, 0x12, 0x0e, 0x0a, 0x02, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6f,
	0x72, 0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x6d, 0x65, 0x63, 0x68, 0x61, 0x6e,
	0x69, 0x63, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x6d, 0x65, 0x63,
	0x68, 0x61, 0x6e, 0x69, 0x63, 0x49, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x69, 0x6c, 0x65, 0x61,
	0x67, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x6d, 0x69, 0x6c, 0x65, 0x61, 0x67,
	0x65, 0x12, 0x10, 0x0a, 0x03, 0x67, 0x61, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52, 0x03,
	0x67, 0x61, 0x73, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x18, 0x06, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65,
	0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70,
	0x64, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74, 0x22, 0x8d, 0x01, 0x0a, 0x0d, 0x43, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x47, 0x69, 0x76, 0x65, 0x43, 0x61, 0x72, 0x12, 0x19, 0x0a, 0x08, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x6d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x63,
	0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x6d, 0x65, 0x63, 0x68, 0x61,
	0x6e, 0x69, 0x63, 0x49, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x69, 0x6c, 0x65, 0x61, 0x67, 0x65,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x6d, 0x69, 0x6c, 0x65, 0x61, 0x67, 0x65, 0x12,
	0x10, 0x0a, 0x03, 0x67, 0x61, 0x73, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x03, 0x67, 0x61,
	0x73, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x22, 0x9d, 0x01, 0x0a, 0x0d, 0x55, 0x70, 0x64, 0x61,
	0x74, 0x65, 0x47, 0x69, 0x76, 0x65, 0x43, 0x61, 0x72, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6f, 0x72, 0x64,
	0x65, 0x72, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x6d, 0x65, 0x63, 0x68, 0x61, 0x6e, 0x69, 0x63,
	0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x6d, 0x65, 0x63, 0x68, 0x61,
	0x6e, 0x69, 0x63, 0x49, 0x64, 0x12, 0x18, 0x0a, 0x07, 0x6d, 0x69, 0x6c, 0x65, 0x61, 0x67, 0x65,
	0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x6d, 0x69, 0x6c, 0x65, 0x61, 0x67, 0x65, 0x12,
	0x10, 0x0a, 0x03, 0x67, 0x61, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52, 0x03, 0x67, 0x61,
	0x73, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x05, 0x70, 0x68, 0x6f, 0x74, 0x6f, 0x22, 0x23, 0x0a, 0x11, 0x47, 0x69, 0x76, 0x65, 0x43,
	0x61, 0x72, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x22, 0x5d, 0x0a, 0x15,
	0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x47, 0x69, 0x76, 0x65, 0x43, 0x61, 0x72, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a,
	0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69,
	0x6d, 0x69, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x62, 0x0a, 0x16, 0x47,
	0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x47, 0x69, 0x76, 0x65, 0x43, 0x61, 0x72, 0x52, 0x65, 0x73,
	0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x32, 0x0a, 0x08, 0x47,
	0x69, 0x76, 0x65, 0x43, 0x61, 0x72, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x16, 0x2e,
	0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x47, 0x69,
	0x76, 0x65, 0x43, 0x61, 0x72, 0x52, 0x08, 0x47, 0x69, 0x76, 0x65, 0x43, 0x61, 0x72, 0x73, 0x42,
	0x18, 0x5a, 0x16, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x6f, 0x72, 0x64, 0x65,
	0x72, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x33,
}

var (
	file_give_car_proto_rawDescOnce sync.Once
	file_give_car_proto_rawDescData = file_give_car_proto_rawDesc
)

func file_give_car_proto_rawDescGZIP() []byte {
	file_give_car_proto_rawDescOnce.Do(func() {
		file_give_car_proto_rawDescData = protoimpl.X.CompressGZIP(file_give_car_proto_rawDescData)
	})
	return file_give_car_proto_rawDescData
}

var file_give_car_proto_msgTypes = make([]protoimpl.MessageInfo, 6)
var file_give_car_proto_goTypes = []interface{}{
	(*GiveCar)(nil),                // 0: order_service.GiveCar
	(*CreateGiveCar)(nil),          // 1: order_service.CreateGiveCar
	(*UpdateGiveCar)(nil),          // 2: order_service.UpdateGiveCar
	(*GiveCarPrimaryKey)(nil),      // 3: order_service.GiveCarPrimaryKey
	(*GetListGiveCarRequest)(nil),  // 4: order_service.GetListGiveCarRequest
	(*GetListGiveCarResponse)(nil), // 5: order_service.GetListGiveCarResponse
}
var file_give_car_proto_depIdxs = []int32{
	0, // 0: order_service.GetListGiveCarResponse.GiveCars:type_name -> order_service.GiveCar
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_give_car_proto_init() }
func file_give_car_proto_init() {
	if File_give_car_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_give_car_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GiveCar); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_give_car_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateGiveCar); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_give_car_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateGiveCar); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_give_car_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GiveCarPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_give_car_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListGiveCarRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_give_car_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListGiveCarResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_give_car_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   6,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_give_car_proto_goTypes,
		DependencyIndexes: file_give_car_proto_depIdxs,
		MessageInfos:      file_give_car_proto_msgTypes,
	}.Build()
	File_give_car_proto = out.File
	file_give_car_proto_rawDesc = nil
	file_give_car_proto_goTypes = nil
	file_give_car_proto_depIdxs = nil
}

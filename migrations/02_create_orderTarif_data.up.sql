CREATE TABLE "order_tarif"(
    "id" UUID PRIMARY KEY,
    "tarif_payment" INT NOT NULL ,
    "investor" INT NOT NULL,
    "insurance" INT NOT NULL,
    "company" INT NOT NULL,
    "day" INT NOT NULL ,
    "order_id" UUID,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
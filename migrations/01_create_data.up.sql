CREATE TABLE "order"(
    "id" UUID PRIMARY KEY,
    "tarif_id" UUID ,
    "client_id" UUID,
    "date" VARCHAR NOT NULL,
    "duration" INT NOT NULL,
    "extend" BOOLEAN ,
    "discount" NUMERIC NOT NULL,
    "insurance" BOOLEAN,
    "branch_id" UUID ,
    "status" VARCHAR NOT NULL,
    "order_number" VARCHAR,
    "payd" BOOLEAN,
    "car_id" UUID,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);
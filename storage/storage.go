package storage

import (
	"context"

	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
)

type StorageI interface {
	CloseDB()
	Order() OrderRepoI
	OrderPayment() OrderPaymentRepoI
	Tarif() TarifRepoI
	OrderTarif() OrderTarifRepoI
	GiveCar() GiveCarRepoI
	ReceiveCar() ReceiveCarRepoI

	Deptor() DeptorRepoI
}

type OrderRepoI interface {
	Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.OrderPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error)
	GetAll(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error)
	Update(ctx context.Context, req *order_service.UpdateOrder) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *order_service.OrderPrimaryKey) (*order_service.OrderEmpty, error)
}
type OrderPaymentRepoI interface {
	Create(ctx context.Context, req *order_service.CreateOrderPayment) (resp *order_service.OrderPaymentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *order_service.OrderPaymentPrimaryKey) (resp *order_service.OrderPayment, err error)
	GetAll(ctx context.Context, req *order_service.GetListOrderPaymentRequest) (resp *order_service.GetListOrderPaymentResponse, err error)
	Update(ctx context.Context, req *order_service.UpdateOrderPayment) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *order_service.OrderPaymentPrimaryKey) (*order_service.OrderPaymentEmpty, error)
}
type TarifRepoI interface {
	Create(ctx context.Context, req *order_service.CreateTarif) (resp *order_service.TarifPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *order_service.TarifPrimaryKey) (resp *order_service.Tarif, err error)
	GetAll(ctx context.Context, req *order_service.GetListTarifRequest) (resp *order_service.GetListTarifResponse, err error)
	Update(ctx context.Context, req *order_service.UpdateTarif) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *order_service.TarifPrimaryKey) (*order_service.TarifEmpty, error)
}
type OrderTarifRepoI interface {
	Create(ctx context.Context, req *order_service.CreateOrderTarif) (resp *order_service.OrderTarifPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *order_service.OrderTarifPrimaryKey) (resp *order_service.OrderTarif, err error)
}

type GiveCarRepoI interface {
	Create(ctx context.Context, req *order_service.CreateGiveCar) (resp *order_service.GiveCarPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *order_service.GiveCarPrimaryKey) (resp *order_service.GiveCar, err error)
	GetAll(ctx context.Context, req *order_service.GetListGiveCarRequest) (resp *order_service.GetListGiveCarResponse, err error)
	Update(ctx context.Context, req *order_service.UpdateGiveCar) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *order_service.GiveCarPrimaryKey) (*order_service.GiveCarEmpty, error)
}
type ReceiveCarRepoI interface {
	Create(ctx context.Context, req *order_service.CreateReceiveCar) (resp *order_service.ReceiveCarPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *order_service.ReceiveCarPrimaryKey) (resp *order_service.ReceiveCar, err error)
	GetAll(ctx context.Context, req *order_service.GetListReceiveCarRequest) (resp *order_service.GetListReceiveCarResponse, err error)
	Update(ctx context.Context, req *order_service.UpdateReceiveCar) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *order_service.ReceiveCarPrimaryKey) (*order_service.ReceiveCarEmpty, error)
}

type DeptorRepoI interface {
	Create(ctx context.Context, req *order_service.CreateDeptor) (resp *order_service.DeptorPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *order_service.DeptorPrimaryKey) (resp *order_service.Deptor, err error)
	GetAll(ctx context.Context, req *order_service.GetListDeptorRequest) (resp *order_service.GetListDeptorResponse, err error)
	Update(ctx context.Context, req *order_service.UpdateDeptor) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *order_service.DeptorPrimaryKey) (*order_service.DeptorEmpty, error)
}

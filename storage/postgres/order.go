package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/helper"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
)

type OrderRepo struct {
	db *pgxpool.Pool
}

func NewOrderRepo(db *pgxpool.Pool) storage.OrderRepoI {
	return &OrderRepo{
		db: db,
	}
}

func (c *OrderRepo) Create(ctx context.Context, req *order_service.CreateOrder) (resp *order_service.OrderPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "order" (
				id,
				tarif_id,   
				client_id ,  
				date ,       
				duration   , 
				extend ,     
				discount ,   
				insurance ,  
				branch_id  , 
				status ,     
				order_number,
				payd   ,     
				car_id 
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10 ,$11, $12, $13)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.TarifId,
		req.ClientId,
		req.Date,
		req.Duration,
		req.Extend,
		req.Discount,
		req.Insurance,
		req.BranchId,
		"New",
		req.OrderNumber,
		req.Payd,
		req.CarId,
	)
	if err != nil {
		return nil, err
	}

	return &order_service.OrderPrimaryKey{Id: id.String()}, nil

}

func (c *OrderRepo) GetByPKey(ctx context.Context, req *order_service.OrderPrimaryKey) (resp *order_service.Order, err error) {
	query := `
		SELECT
			id,
			tarif_id,   
			client_id ,  
			date ,       
			duration   , 
			extend ,     
			discount ,   
			insurance ,  
			branch_id  , 
			status ,     
			order_number,
			payd   ,     
			car_id  ,    
			created_at,
			updated_at
		FROM "order"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		tarif_id     sql.NullString
		client_id    sql.NullString
		date         sql.NullString
		duration     int
		extend       bool
		discount     int
		insuranse    bool
		branch_id    sql.NullString
		status       sql.NullString
		order_number sql.NullString
		payd         bool
		car_id       sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&tarif_id,
		&client_id,
		&date,
		&duration,
		&extend,
		&discount,
		&insuranse,
		&branch_id,
		&status,
		&order_number,
		&payd,
		&car_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &order_service.Order{
		Id:          id.String,
		TarifId:     tarif_id.String,
		ClientId:    client_id.String,
		Date:        date.String,
		Duration:    int64(duration),
		Extend:      bool(extend),
		Discount:    int64(discount),
		Insurance:   insuranse,
		BranchId:    branch_id.String,
		Status:      status.String,
		OrderNumber: order_number.String,
		Payd:        bool(payd),
		CarId:       car_id.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *OrderRepo) GetAll(ctx context.Context, req *order_service.GetListOrderRequest) (resp *order_service.GetListOrderResponse, err error) {

	resp = &order_service.GetListOrderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			tarif_id,  
			client_id,  
			date,
			duration,
			extend,
			discount,
			insurance,
			branch_id,
			status,
			order_number,
			payd,
			car_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "order"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			tarif_id     sql.NullString
			client_id    sql.NullString
			date         sql.NullString
			duration     int
			extend       bool
			discount     int
			insuranse    bool
			branch_id    sql.NullString
			status       sql.NullString
			order_number sql.NullString
			payd         bool
			car_id       sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&tarif_id,
			&client_id,
			&date,
			&duration,
			&extend,
			&discount,
			&insuranse,
			&branch_id,
			&status,
			&order_number,
			&payd,
			&car_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Orders = append(resp.Orders, &order_service.Order{
			Id:          id.String,
			TarifId:     tarif_id.String,
			ClientId:    client_id.String,
			Date:        date.String,
			Duration:    int64(duration),
			Extend:      bool(extend),
			Discount:    int64(discount),
			Insurance:   insuranse,
			BranchId:    branch_id.String,
			Status:      status.String,
			OrderNumber: order_number.String,
			Payd:        payd,
			CarId:       car_id.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *OrderRepo) Update(ctx context.Context, req *order_service.UpdateOrder) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "order"
			SET
				tarif_id = :tarif_id,
				client_id = :client_id,
				date = :date,
				duration = :duration,
				extend = :extend,
				discount = :discount,
				insurance = :insurance,
				branch_id = :branch_id,
				status = :status,
				order_number = :order_number,
				payd = :payd,
				car_id = :car_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"tarif_id":     req.GetTarifId(),
		"client_id":    req.GetClientId(),
		"date":         req.GetDate(),
		"duration":     req.GetDuration(),
		"extend":       req.GetExtend(),
		"discount":     req.GetDiscount(),
		"insurance":    req.GetInsurance(),
		"branch_id":    req.GetBranchId(),
		"status":       req.GetStatus(),
		"order_number": req.GetOrderNumber(),
		"payd":         req.GetPayd(),
		"car_id":       req.GetCarId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *OrderRepo) Delete(ctx context.Context, req *order_service.OrderPrimaryKey) (*order_service.OrderEmpty, error) {

	query := `DELETE FROM "order" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &order_service.OrderEmpty{}, nil
}

package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/helper"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
)

type ReceiveCarRepo struct {
	db *pgxpool.Pool
}

func NewReceiveCarRepo(db *pgxpool.Pool) storage.ReceiveCarRepoI {
	return &ReceiveCarRepo{
		db: db,
	}
}

func (c *ReceiveCarRepo) Create(ctx context.Context, req *order_service.CreateReceiveCar) (resp *order_service.ReceiveCarPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "Receive_car" (
				id,
				order_id,
				mechanic_id,
				mileage,
				gas,
				photo
			) VALUES ($1, $2, $3, $4, $5, $6)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.OrderId,
		req.MechanicId,
		req.Mileage,
		req.Gas,
		req.Photo,
	)
	if err != nil {
		return nil, err
	}

	return &order_service.ReceiveCarPrimaryKey{Id: id.String()}, nil

}

func (c *ReceiveCarRepo) GetByPKey(ctx context.Context, req *order_service.ReceiveCarPrimaryKey) (resp *order_service.ReceiveCar, err error) {
	query := `
		SELECT
			id,
			order_id,
			mechanic_id,
			mileage,
			gas,
			photo,
			created_at,
			updated_at
		FROM "Receive_car"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		order_id    sql.NullString
		mechanic_id sql.NullString
		milleage    int
		gas         int
		photo       sql.NullString
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&order_id,
		&mechanic_id,
		&milleage,
		&gas,
		&photo,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &order_service.ReceiveCar{
		Id:         id.String,
		OrderId:    order_id.String,
		MechanicId: mechanic_id.String,
		Mileage:    int64(milleage),
		Gas:        int64(gas),
		Photo:      photo.String,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *ReceiveCarRepo) GetAll(ctx context.Context, req *order_service.GetListReceiveCarRequest) (resp *order_service.GetListReceiveCarResponse, err error) {

	resp = &order_service.GetListReceiveCarResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			order_id,
			mechanic_id,
			mileage,
			gas,
			photo,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "Receive_car"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}
	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			order_id    sql.NullString
			mechanic_id sql.NullString
			milleage    int
			gas         int
			photo       sql.NullString
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&order_id,
			&mechanic_id,
			&milleage,
			&gas,
			&photo,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.ReceiveCars = append(resp.ReceiveCars, &order_service.ReceiveCar{
			Id:         id.String,
			OrderId:    order_id.String,
			MechanicId: mechanic_id.String,
			Mileage:    int64(milleage),
			Gas:        int64(gas),
			Photo:      photo.String,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *ReceiveCarRepo) Update(ctx context.Context, req *order_service.UpdateReceiveCar) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "Receive_car"
			SET
				order_id = :order_id,
				mechanic_id = :mechanic_id,
				mileage = :mileage,
				gas = :gas,
				photo = :photo,
				status = :status,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"order_id":    req.GetOrderId(),
		"mechanic_id": req.GetMechanicId(),
		"mileage":     req.GetMileage(),
		"gas":         req.GetGas(),
		"photo":       req.GetPhoto(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ReceiveCarRepo) Delete(ctx context.Context, req *order_service.ReceiveCarPrimaryKey) (*order_service.ReceiveCarEmpty, error) {

	query := `DELETE FROM "Receive_car" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &order_service.ReceiveCarEmpty{}, nil
}

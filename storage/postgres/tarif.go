package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/helper"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
)

type TarifRepo struct {
	db *pgxpool.Pool
}

func NewTarifRepo(db *pgxpool.Pool) storage.TarifRepoI {
	return &TarifRepo{
		db: db,
	}
}

func (c *TarifRepo) Create(ctx context.Context, req *order_service.CreateTarif) (resp *order_service.TarifPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "tarif" (
				id,
				photo,   
				model_id ,  
				name ,       
				price   , 
				day_limit ,     
				over_limit ,   
				insurance_price ,  
				description  , 
				status 
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Photo,
		req.ModelId,
		req.Name,
		req.Price,
		req.DayLimit,
		req.OverLimit,
		req.InsurancePrice,
		req.Description,
		req.Status,
	)
	if err != nil {
		return nil, err
	}

	return &order_service.TarifPrimaryKey{Id: id.String()}, nil

}

func (c *TarifRepo) GetByPKey(ctx context.Context, req *order_service.TarifPrimaryKey) (resp *order_service.Tarif, err error) {
	query := `
		SELECT
			id,
			photo,   
			model_id ,  
			name ,       
			price   , 
			day_limit ,     
			over_limit ,   
			insurance_price ,  
			description  , 
			status,     
			created_at,
			updated_at
		FROM "tarif"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		photo       sql.NullString
		model_id    sql.NullString
		name        sql.NullString
		price       int
		day_limit   int
		over_limit  int
		insuranse   int
		description sql.NullString
		status      bool
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&photo,
		&model_id,
		&name,
		&price,
		&day_limit,
		&over_limit,
		&insuranse,
		&description,
		&status,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &order_service.Tarif{
		Id:             id.String,
		ModelId:        model_id.String,
		Photo:          photo.String,
		Price:          int64(price),
		DayLimit:       int64(day_limit),
		Name:           name.String,
		OverLimit:      int64(over_limit),
		InsurancePrice: int64(insuranse),
		Description:    description.String,
		Status:         status,
		CreatedAt:      createdAt.String,
		UpdatedAt:      updatedAt.String,
	}

	return
}

func (c *TarifRepo) GetAll(ctx context.Context, req *order_service.GetListTarifRequest) (resp *order_service.GetListTarifResponse, err error) {

	resp = &order_service.GetListTarifResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " Order BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,  
			photo,
			model_id, 
			name,
			price,
			day_limit,
			over_limit,
			insurance_price,
			description,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "tarif"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			photo       sql.NullString
			model_id    sql.NullString
			name        sql.NullString
			price       int
			day_limit   int
			over_limit  int
			insuranse   int
			description sql.NullString
			status      bool
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&photo,
			&model_id,
			&name,
			&price,
			&day_limit,
			&over_limit,
			&insuranse,
			&description,
			&status,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Tarifs = append(resp.Tarifs, &order_service.Tarif{
			Id:             id.String,
			ModelId:        model_id.String,
			Photo:          photo.String,
			Price:          int64(price),
			DayLimit:       int64(day_limit),
			Name:           name.String,
			OverLimit:      int64(over_limit),
			InsurancePrice: int64(insuranse),
			Description:    description.String,
			Status:         status,
			CreatedAt:      createdAt.String,
			UpdatedAt:      updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *TarifRepo) Update(ctx context.Context, req *order_service.UpdateTarif) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "tarif"
			SET
				name = :name,
				photo = :photo,
				model_id = :model_id,
				price = :price,
				day_limit = :day_limit,
				over_limit = :over_limit,
				insurance_price = :insurance_price,
				description = :description,
				status = :status,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":              req.GetId(),
		"name":            req.GetName(),
		"photo":           req.GetPhoto(),
		"model_id":        req.GetModelId(),
		"price":           req.GetPrice(),
		"day_limit":       req.GetDayLimit(),
		"over_limit":      req.GetOverLimit(),
		"insurance_price": req.GetInsurancePrice(),
		"description":     req.GetDescription(),
		"status":          req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *TarifRepo) Delete(ctx context.Context, req *order_service.TarifPrimaryKey) (*order_service.TarifEmpty, error) {

	query := `DELETE FROM "tarif" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &order_service.TarifEmpty{}, nil
}

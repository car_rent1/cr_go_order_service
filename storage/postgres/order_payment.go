package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/helper"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
)

type OrderPaymentRepo struct {
	db *pgxpool.Pool
}

func NewOrderPaymentRepo(db *pgxpool.Pool) storage.OrderPaymentRepoI {
	return &OrderPaymentRepo{
		db: db,
	}
}

func (c *OrderPaymentRepo) Create(ctx context.Context, req *order_service.CreateOrderPayment) (resp *order_service.OrderPaymentPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "order_payment" (
				id,
				order_id,   
				cash, 
				humo,
				uzcard,
				click,
				payme
			) VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.OrderId,
		req.Cash,
		req.Humo,
		req.Uzcard,
		req.Click,
		req.Payme,
	)
	if err != nil {
		return nil, err
	}

	return &order_service.OrderPaymentPrimaryKey{Id: id.String()}, nil

}

func (c *OrderPaymentRepo) GetByPKey(ctx context.Context, req *order_service.OrderPaymentPrimaryKey) (resp *order_service.OrderPayment, err error) {
	query := `
		SELECT
			id,
			order_id,   
			cash, 
			humo,
			uzcard,
			click,
			payme,   
			created_at,
			updated_at
		FROM "order_payment"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		order_id  sql.NullString
		cash      int
		humo      int
		uzcard    int
		click     int
		payme     int
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&order_id,
		&cash,
		&humo,
		&uzcard,
		&click,
		&payme,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &order_service.OrderPayment{
		Id:        id.String,
		OrderId:   order_id.String,
		Cash:      int64(cash),
		Humo:      int64(humo),
		Uzcard:    int64(uzcard),
		Click:     int64(click),
		Payme:     int64(payme),
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *OrderPaymentRepo) GetAll(ctx context.Context, req *order_service.GetListOrderPaymentRequest) (resp *order_service.GetListOrderPaymentResponse, err error) {

	resp = &order_service.GetListOrderPaymentResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			order_id,   
			cash, 
			humo,
			uzcard,
			click,
			payme,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "order_payment"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			order_id  sql.NullString
			cash      int
			humo      int
			uzcard    int
			click     int
			payme     int
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&order_id,
			&cash,
			&humo,
			&uzcard,
			&click,
			&payme,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.OrderPayments = append(resp.OrderPayments, &order_service.OrderPayment{
			Id:        id.String,
			OrderId:   order_id.String,
			Cash:      int64(cash),
			Humo:      int64(humo),
			Uzcard:    int64(uzcard),
			Click:     int64(click),
			Payme:     int64(payme),
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *OrderPaymentRepo) Update(ctx context.Context, req *order_service.UpdateOrderPayment) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "order_payment"
			SET
				order_id = :order_id,
				cash = :cash,
				humo = :humo,
				uzcard = :uzcard,
				click = :click,
				payme = :payme,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":       req.GetId(),
		"order_id": req.GetOrderId(),
		"cash":     req.GetCash(),
		"humo":     req.GetHumo(),
		"uzcard":   req.GetUzcard(),
		"click": req.GetClick(),
		"payme": req.GetPayme(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *OrderPaymentRepo) Delete(ctx context.Context, req *order_service.OrderPaymentPrimaryKey) (*order_service.OrderPaymentEmpty, error) {

	query := `DELETE FROM "order_payment" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &order_service.OrderPaymentEmpty{}, nil
}

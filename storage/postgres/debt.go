package postgres

import (
	"context"
	"database/sql"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/pkg/helper"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
)

type DeptorRepo struct {
	db *pgxpool.Pool
}

func NewDeptorRepo(db *pgxpool.Pool) storage.DeptorRepoI {
	return &DeptorRepo{
		db: db,
	}
}

func (c *DeptorRepo) Create(ctx context.Context, req *order_service.CreateDeptor) (resp *order_service.DeptorPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "deptor" (
				id,
				order_id,   
				dept_sum,
				status
			) VALUES ($1, $2, $3)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.OrderId,
		req.DeptSum,
		req.Status,
	)
	if err != nil {
		return nil, err
	}

	return &order_service.DeptorPrimaryKey{Id: id.String()}, nil

}

func (c *DeptorRepo) GetByPKey(ctx context.Context, req *order_service.DeptorPrimaryKey) (resp *order_service.Deptor, err error) {
	query := `
		SELECT
			id,
			order_id,   
			dept_sum,
			status, 
			created_at,
			updated_at
		FROM "deptor"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		order_id  sql.NullString
		dept_sum  sql.NullInt64
		status    sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&order_id,
		&dept_sum,
		&status,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &order_service.Deptor{
		Id:        id.String,
		OrderId:   order_id.String,
		DeptSum:   dept_sum.Int64,
		Status:    status.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *DeptorRepo) GetAll(ctx context.Context, req *order_service.GetListDeptorRequest) (resp *order_service.GetListDeptorResponse, err error) {

	resp = &order_service.GetListDeptorResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			order_id,   
			dept_sum,
			status,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "deptor"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.SearchByOrderID)>0{
		searchByOrderID := " and order_id :order_id"
		params["order_id"]= req.SearchByOrderID
		filter+= searchByOrderID
	}

	if len(req.SearchByStatus)>0{
		searchByStatus := " and status :status"
		params["status"]= req.SearchByStatus
		filter+= searchByStatus
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			order_id  sql.NullString
			dept_sum  sql.NullInt64
			status    sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&order_id,
			&dept_sum,
			&status,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Deptors = append(resp.Deptors, &order_service.Deptor{
			Id:        id.String,
			OrderId:   order_id.String,
			DeptSum:   dept_sum.Int64,
			Status:    status.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *DeptorRepo) Update(ctx context.Context, req *order_service.UpdateDeptor) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "deptor"
			SET
				order_id = :order_id,
				dept_sum = :dept_sum,
				status = :status,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":       req.GetId(),
		"order_id": req.GetOrderId(),
		"dept_sum": req.GetDeptSum(),
		"status": req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *DeptorRepo) Delete(ctx context.Context, req *order_service.DeptorPrimaryKey) (*order_service.DeptorEmpty, error) {

	query := `DELETE FROM "deptor" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return &order_service.DeptorEmpty{}, nil
}


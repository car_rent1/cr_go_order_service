package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_order_service/config"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
)

type Store struct {
	db           *pgxpool.Pool
	order        storage.OrderRepoI
	orderpayment storage.OrderPaymentRepoI
	tarif        storage.TarifRepoI
	ordertarif   storage.OrderTarifRepoI
	giveCar      storage.GiveCarRepoI
	receivecar   storage.ReceiveCarRepoI
	deptor       storage.DeptorRepoI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",

			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err

}
func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Order() storage.OrderRepoI {
	if s.order == nil {
		s.order = NewOrderRepo(s.db)
	}
	return s.order
}

func (s *Store) OrderPayment() storage.OrderPaymentRepoI {
	if s.orderpayment == nil {
		s.orderpayment = NewOrderPaymentRepo(s.db)
	}
	return s.orderpayment
}

func (s *Store) Tarif() storage.TarifRepoI {
	if s.tarif == nil {
		s.tarif = NewTarifRepo(s.db)
	}
	return s.tarif
}

func (s *Store) OrderTarif() storage.OrderTarifRepoI {
	if s.ordertarif == nil {
		s.ordertarif = NewOrderTarifRepo(s.db)
	}
	return s.ordertarif
}
func (s *Store) GiveCar() storage.GiveCarRepoI {
	if s.giveCar == nil {
		s.giveCar = NewGiveCarRepo(s.db)
	}
	return s.giveCar
}

func (s *Store) ReceiveCar() storage.ReceiveCarRepoI {
	if s.receivecar == nil {
		s.receivecar = NewReceiveCarRepo(s.db)
	}
	return s.receivecar
}


func (s *Store) Deptor() storage.DeptorRepoI {
	if s.deptor == nil {
		s.deptor = NewDeptorRepo(s.db)
	}
	return s.deptor
}
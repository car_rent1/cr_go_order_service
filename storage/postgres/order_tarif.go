package postgres

import (
	"context"
	"database/sql"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/car_rent1/cr_go_order_service/genproto/order_service"
	"gitlab.com/car_rent1/cr_go_order_service/storage"
)

type OrderTarifRepo struct {
	db *pgxpool.Pool
}

func NewOrderTarifRepo(db *pgxpool.Pool) storage.OrderTarifRepoI {
	return &OrderTarifRepo{
		db: db,
	}
}

func (c *OrderTarifRepo) Create(ctx context.Context, req *order_service.CreateOrderTarif) (resp *order_service.OrderTarifPrimaryKey, err error) {
	query := `INSERT INTO "order_tarif" (
				id,
				tarif_payment,   
				investor ,  
				insurance ,       
				company   , 
				day ,     
				order_id 
			) VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	_, err = c.db.Exec(ctx,
		query,
		req.Id,
		req.TarifPayment,
		req.Investor,
		req.Insurance,
		req.Company,
		req.Day,
		req.OrderId,
	)
	if err != nil {
		return nil, err
	}

	return &order_service.OrderTarifPrimaryKey{Id: req.Id}, nil

}
func (c *OrderTarifRepo) GetByPKey(ctx context.Context, req *order_service.OrderTarifPrimaryKey) (resp *order_service.OrderTarif, err error) {
	query := `
		SELECT
			id,
			tarif_payment,   
			investor ,  
			insurance ,       
			company   , 
			day ,     
			order_id ,       
			created_at
		FROM "order_tarif"
		WHERE id = $1
	`

	var (
		id            sql.NullString
		investor      int
		tarif_payment int
		insurance     int
		company       int
		day           int
		order_id      sql.NullString
		createdAt     sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&tarif_payment,
		&investor,
		&insurance,
		&company,
		&day,
		&order_id,
		&createdAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &order_service.OrderTarif{
		Id:           req.Id,
		TarifPayment: int64(tarif_payment),
		Investor:     int64(investor),
		Insurance:    int64(insurance),
		Company:      int64(company),
		Day:          int64(day),
		OrderId:      order_id.String,
		CreatedAt:    createdAt.String,
	}

	return
}
